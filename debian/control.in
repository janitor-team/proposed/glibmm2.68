Source: glibmm2.68
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome,
               doxygen,
               glib-networking <!nocheck>,
               graphviz,
               xsltproc,
               libglib2.0-dev (>= 2.71.2),
               libsigc++-3.0-dev,
               libxml-parser-perl <!nodoc>,
               meson (>= 0.55.0),
               pkg-config,
               mm-common (>= 0.9.10)
Standards-Version: 4.6.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/gnome-team/glibmm2.68
Vcs-Git: https://salsa.debian.org/gnome-team/glibmm2.68.git
Homepage: http://www.gtkmm.org/

Package: libglibmm-2.68-1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: C++ wrapper for the GLib toolkit (shared libraries)
 GLib is a low-level general-purpose library used mainly by GTK+/GNOME
 applications, but is useful for other programs as well.
 glibmm is the C++ wrapper for GLib.
 .
 This package contains shared libraries.

Package: libglibmm-2.68-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libglibmm-2.68-1 (= ${binary:Version}),
         libglib2.0-dev (>= 2.71.2),
         libsigc++-3.0-dev,
         pkg-config
Suggests: libglibmm-2.68-doc,
          libgtkmm-4.0-dev
Description: C++ wrapper for the GLib toolkit (development files)
 GLib is a low-level general-purpose library used mainly by GTK+/GNOME
 applications, but is useful for other programs as well.
 glibmm is the C++ wrapper for GLib.
 .
 This package contains development files.

Package: libglibmm-2.68-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         doc-base,
Suggests: devhelp, gtkmm-documentation
Description: C++ wrapper for the GLib toolkit (documentation)
 GLib is a low-level general-purpose library used mainly by GTK+/GNOME
 applications, but is useful for other programs as well.
 glibmm is the C++ wrapper for GLib.
 .
 This package contains reference documentation and examples.
